package controller;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import model.Player;
import utils.GameGrid;

public class Controller {
	@FXML
	GridPane gridPane;

	@FXML
	Button buttonMove;

	Player player;
	GameGrid gameGrid;
	
	@FXML
	public void initialize() {
		player = new Player("Max");
		gameGrid = new GameGrid(gridPane);
		addEventHandler();
		drawPlayer();
	}
		

	private void addEventHandler() {
		for(AnchorPane a : gameGrid.getBorderElements()){
			Label label = new Label("Cklick me");
			AnchorPane.setBottomAnchor(label, 0d);
			a.getChildren().add(label);
			a.setOnMouseClicked(new EventHandler<MouseEvent>() {
			    public void handle(MouseEvent me) {
			    	Alert alert = new Alert(AlertType.INFORMATION);
			    	alert.setTitle("Information Dialog");
			    	alert.setContentText("You clicked #" + gameGrid.getBorderElements().indexOf(a));
			    	alert.showAndWait();
			    }
			});
		}
		
	}


	public void drawPlayer(){
		gameGrid.getElement(player.getPosition()).getChildren().add(player.getImageView());
	
	}

	@FXML
	public void move(){
		int position = player.getPosition();
		position++;
		//begin at zero after passing start
		position = position%gameGrid.getBorderElements().size();
		player.setPosition(position);
		drawPlayer();
	}

}
