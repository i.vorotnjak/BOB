package model;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Player {
	private String name;
	private int position;
	private ImageView imageView;

	public Player(String name) {
		super();
		this.name = name;
		this.position = 0;
		String url = getClass().getResource("/resources/mario.png").toString();
		this.imageView = new ImageView(new Image(url, 60, 0, true, false));
	}
	
	

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public ImageView getImageView() {
		return imageView;
	}

}
