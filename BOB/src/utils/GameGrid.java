package utils;

import java.util.ArrayList;
import java.util.List;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;

public class GameGrid {
	private List<AnchorPane> borderElements;
	private GridPane grid;

	public GameGrid(GridPane grid) {
		super();
		this.grid = grid;
		borderElements = new ArrayList<AnchorPane>();
		addElements();
	}
	
	private void addElements(){
		int lastCol = numCols()-1;
		int lastRow = numRows()-1;
		for(int c = 0; c <= lastCol; c++){
			AnchorPane pane = new AnchorPane();
			borderElements.add(pane);
			grid.add(pane, c, 0);
		}
		for(int r = 1; r <= lastRow; r++){
			AnchorPane pane = new AnchorPane();
			borderElements.add(pane);
			grid.add(pane, lastCol, r);
		}
		for(int c = lastCol-1; c > 0; c--){
			AnchorPane pane = new AnchorPane();
			borderElements.add(pane);
			grid.add(pane, c, lastRow);
		}
		for(int r = lastRow-1; r > 0; r--){
			AnchorPane pane = new AnchorPane();
			borderElements.add(pane);
			grid.add(pane, 0, r);
		}	
	}

	private int numRows() {
		return grid.getRowConstraints().size();
	}

	private int numCols() {
		return grid.getColumnConstraints().size();
	}

	public AnchorPane getElement(int position) {
		return borderElements.get(position);
	}

	public List<AnchorPane> getBorderElements() {
		return borderElements;
	}

}
